year = input("Please enter a valid year: \n")
if year.isnumeric() == True:
	year = int(year)
	if year >= 0:
		if year % 4 == 0:
			print(f"{year} is a leap year \n")
		else:
			print(f"{year} is not a leap year \n")
	else:
		print("Error")
else:
	print("error")
row = int(input("Enter number of rows: \n"))
column = int(input("Enter number of columns: \n"))

for x in range(row):
	for y in range(column):
		print("*", end="")
	print()
	